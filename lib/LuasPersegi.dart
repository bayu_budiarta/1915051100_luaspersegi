import 'package:flutter/material.dart';
import 'LuasPersegi.dart';
class LuasPersegi extends StatefulWidget {
  @override
  _LuasPersegiState createState() => _LuasPersegiState();
}

class _LuasPersegiState extends State<LuasPersegi> {
  double sisi = 0;
  double luas = 0;
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.blue[100],
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Hitung Cepat Persegi',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          backgroundColor: Colors.black87,
          leading: new IconButton(
            icon: new Icon(Icons.menu_open_outlined, color: Colors.white),
            onPressed: () {},
          ),
          actions: <Widget>[
            new IconButton(
              icon: new Icon(Icons.vertical_split, color: Colors.black87),
              onPressed: () {},
            ),
          ],
        ),
        body: Container(
          height: 400,
          margin: const EdgeInsets.all(33),
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Colors.grey.shade100,
              border: Border.all(
                color: Colors.black,
              ),
              borderRadius: BorderRadius.all(Radius.circular(20))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 15,
              ),
              Text(
                "Menghitung Luas Persegi",
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.underline),
              ),
              SizedBox(
                height: 70,
              ),
              Text("Sisi :  "),
              Expanded(
                  child: TextField(
                    onChanged: (txt) {
                      setState(() {
                        sisi = double.parse(txt);
                      });
                    },
                    keyboardType: TextInputType.number,
                    maxLength: 7,
                  )),
              SizedBox(
                height: 13,
              ),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    luas = sisi * sisi;
                  });
                },
                child: Text("Hitung"),
              ),
              SizedBox(
                height: 15,
              ),
              Text("Luas dari persegi dengan sisi : $sisi"),
              SizedBox(
                height: 5,
              ),
              Text(
                "HASIL : $luas",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 15,
              ),
            ],
          ),
        ),
      ),
    );
  }
}